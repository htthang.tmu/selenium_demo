package test.template.testCases.register;

import org.testng.annotations.Test;
import test.template.testCases.base.BaseWebTestCase;


public class RegisterAccountWithoutDataObject extends BaseWebTestCase {

    @Test
    public void LaunchBrowser() {
        browserURL("https://alada.vn/tai-khoan/dang-ky.html");
    }

    @Test(priority = 1)
    public void register_with_empty_data() {
        registerObject.clickOnSubmit();

        registerObject.verifyNameWarningMessage("Vui lòng nhập họ tên");
        registerObject.verifyEmailWarningMessage("Vui lòng nhập email");
        registerObject.verifyConfirmEmailWarningMessage("Vui lòng nhập lại địa chỉ email");
        registerObject.verifyPasswordWarningMessage("Vui lòng nhập mật khẩu");
        registerObject.verifyConfirmPasswordWarningMessage("Vui lòng nhập lại mật khẩu");
        registerObject.verifyPhoneWarningMessage("Vui lòng nhập số điện thoại.");
    }

    @Test(priority = 2)
    public void Register_With_Email_Invalidate() {
        registerObject.registerAccountWithoutDataObject("Nancy", "Nancygmail.com", "Nancygmail.com", "yMFZbrrvT8BUd5k", "yMFZbrrvT8BUd5k",
                "0968200852");

        registerObject.verifyEmailWarningMessage("Vui lòng nhập email hợp lệ");
        registerObject.verifyConfirmEmailWarningMessage("Email nhập lại không đúng");
    }

    @Test(priority = 3)
    public void register_with_incorrect_confirm_email() {
        registerObject.registerAccountWithoutDataObject("Nancy", "1Nancy@gmail.com", "Nancy@gmail.com", "yMFZbrrvT8BUd5k", "yMFZbrrvT8BUd5k",
                "0968200852");

        registerObject.verifyConfirmEmailWarningMessage("Email nhập lại không đúng");
    }

    @Test(priority = 4)
    public void register_with_password_less_than_six_characters() {
        registerObject.registerAccountWithoutDataObject("Nancy", "Nancy111@gmail.com", "Nancy111@gmail.com", "1234", "1234", "0968200852");

        registerObject.verifyConfirmPasswordWarningMessage("Mật khẩu phải có ít nhất 6 ký tự");
    }

}
