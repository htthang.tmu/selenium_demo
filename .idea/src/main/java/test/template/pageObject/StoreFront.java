package test.template.PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import test.template.utils.SeleniumWaitUtils;

import java.util.List;
import java.util.Random;

public class StoreFront {
    public static WebDriver driver;
    public static By getProduct= By.xpath("//div[@class=col-6 col-md-3]");
    public static By waitProduct = By.xpath("//div[@class=ProductMain]");
    public static By getItem= By.xpath("//input[@type=file]");
    public static By uploadButton= By.xpath("//div[@class='mt-2 mb-2'][${i}+1]//input[@type='file'] ");
    public static By txtVerifyInput= By.xpath("//textarea[@class='PIText form-control']");
    public static By txtInput= By.xpath("//textarea[@class='PIText form-control']");
    public static By verifyVariant= By.xpath("//div[@class='TypeVariant Variant']");
    public static By selectVariant= By.xpath("//textarea[@class='PIText form-control']");

    public void selectRandomProduct(By elementProduct, By elementWait){
        // Find and click on a random product
        List<WebElement> allProducts = driver.findElements(elementProduct);
        Random rand = new Random();
        int randomProduct = rand.nextInt(allProducts.size());
        allProducts.get(randomProduct).click();
        SeleniumWaitUtils.waitForElement(elementWait);
    }
    public void selectImage(String uploadButton){
        List<WebElement> myElements = driver.findElements(getItem);
        int count = myElements.size();
        System.out.println("The total number of elements is " + myElements.size());
        for (int i=0; i < count; i++) {
            String button = String.format(uploadButton, i);
            driver.findElement(By.xpath(button)).sendKeys();
        }

    }

    public void inputText(By element,By elementCount , String txtInput ){
        Boolean inputText= driver.findElement(element).isDisplayed();
        if(inputText==true){
            List<WebElement> myElements;
            myElements = driver.findElements(elementCount);
            int count = myElements.size();
            System.out.println("The total number of elements is " + myElements.size());
            for (int i=0; i < count; i++) {
                String button = String.format(txtInput, i);
                driver.findElement(By.xpath(button)).sendKeys("AutoTest");
            }
        }
    }

//    public void selectVariant(){
//        Boolean inputText= driver.findElement(element).isDisplayed();
//        if(inputText==true){
//            List<WebElement> myElements;
//            myElements = driver.findElements(elementCount);
//            int count = myElements.size();
//            System.out.println("The total number of elements is " + myElements.size());
//            for (int i=0; i < count; i++) {
//                String button = String.format(txtInput, i);
//                driver.findElement(By.xpath(button)).sendKeys("AutoTest");
//            }
//        }
//
//    }

}

