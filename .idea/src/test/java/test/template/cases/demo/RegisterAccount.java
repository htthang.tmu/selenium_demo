package test.template.cases.demo;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import test.template.cases.base.BaseWebTestCase;
import test.template.dataObjects.AccountInfo;
import test.template.utils.SeleniumUtils;


public class RegisterAccount extends BaseWebTestCase {
    @BeforeTest
    public void beforeMethod() {

        driver().get("https://alada.vn/tai-khoan/dang-ky.html");
        SeleniumUtils.takeScreenShot();
    }

    @Test(priority = 1)
    public void register_with_empty_data() {
        AccountInfo accountInfo = new AccountInfo();
        accountInfo.setEmail("helloworld");

        SeleniumUtils.click(registerObject.btnSubmit);

        // Verify field name is empty
        registerObject.verifyResult(registerObject.verifyName, "Vui lòng nhập họ tên");

        // Verify field email is empty
        registerObject.verifyResult(registerObject.verifyCEmail, "Vui lòng nhập email");

        // Verify field confirm email is empty
        registerObject.verifyResult(registerObject.verifyCEmail, "Vui lòng nhập lại địa chỉ email");

        // Verify field password is empty
        registerObject.verifyResult(registerObject.verifyPass, "Vui lòng nhập mật khẩu");

        // Verify field confirm password is empty
        registerObject.verifyResult(registerObject.verifyCPass, "Vui lòng nhập lại mật khẩu");

        // Verify field phone is empty
        registerObject.verifyResult(registerObject.verifyPhone, "Vui lòng nhập số điện thoại.");

    }

    @Test(priority = 2)
    public void Register_With_Email_Invalidate() {
        registerObject.registerFunction("Nancy", "Nancygmail.com", "Nancygmail.com", "yMFZbrrvT8BUd5k", "yMFZbrrvT8BUd5k", "0968200852");

        // Verify field email is incorrect
        registerObject.verifyResult(registerObject.verifyEmail, "Vui lòng nhập email hợp lệ");

        // Verify field confirm email is incorrect
        registerObject.verifyResult(registerObject.verifyCEmail, "Email nhập lại không đúng");

        SeleniumUtils.refresh();
    }

    @Test(priority = 3)
    public void register_with_incorrect_confirm_email() {
        registerObject.registerFunction("Nancy", "1Nancy@gmail.com", "Nancy@gmail.com", "yMFZbrrvT8BUd5k", "yMFZbrrvT8BUd5k", "0968200852");

        // Verify field confirm email is incorrect
        registerObject.verifyResult(registerObject.verifyCEmail, "Email nhập lại không đúng");
        SeleniumUtils.refresh();
    }

    @Test(priority = 4)
    public void register_with_password_less_than_six_characters() {
        registerObject.registerFunction("Nancy", "Nancy111@gmail.com", "Nancy111@gmail.com", "1234", "1234", "0968200852");

        // Verify field confirm password is invalid
        registerObject.verifyResult(registerObject.verifyPass, "Mật khẩu phải có ít nhất 6 ký tự");
        SeleniumUtils.refresh();
    }

}
