package test.template.pages;

import org.openqa.selenium.By;
import test.template.common.BasePage;
import test.template.utils.SeleniumUtils;

public class LoginHitachiPageObject extends BasePage {
    private By txtUserName = By.id("username");
    private By txtPassword = By.id("password");
    private By btnLogin = By.xpath("//input[@type='submit']");


    public void loginHitachi(String UserName, String Password) {
        inputUserName(UserName);
        inputPassword(Password);
        clickOnSubmit();

    }

    private void inputUserName(String userName) {
        log.info("Input user name: " + userName);
        SeleniumUtils.sendKeys(txtUserName, userName);
    }
    private void inputPassword(String password) {
        log.info("Input password: " + password);
        SeleniumUtils.sendKeys(txtPassword, password);
    }
    public void clickOnSubmit() {
        log.info("Click on login button");
        SeleniumUtils.click(btnLogin);
    }

}

