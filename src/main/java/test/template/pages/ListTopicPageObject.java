package test.template.pages;

import org.openqa.selenium.By;
import test.template.common.BasePage;
import test.template.utils.SeleniumUtils;

public class ListTopicPageObject extends BasePage {
    private By buttonCreatNewTopic = By.xpath("//h4[@class='HeadingFormTopicsTitle']");

    public void clickButtonCreatNewTopic () {
        log.info("Click buttom creat new topic");
        SeleniumUtils.click(buttonCreatNewTopic);
    }
}
