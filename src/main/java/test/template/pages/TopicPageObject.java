package test.template.pages;

import org.openqa.selenium.By;
import test.template.common.BasePage;
import test.template.utils.SeleniumUtils;

public class TopicPageObject extends BasePage {

        private By linkTextTopic = By.xpath("//a[@href ='/topics']");
        private By btnFormTopic = By.xpath("//a[contains(@class,'ButtonAddNewTopics')]");
        private By nameTopic = By.id("nameTopic");
        private By btnSubmitTopic = By.xpath("//input[contains(@class,'ButtonSubmit')]");
        private By listNameTopics= By.xpath("//tbody/tr[*]/td[2]");



        public void clickOnLinkTopic() {
                log.info("Click on link topic at screen home");
                SeleniumUtils.click(linkTextTopic);
        }

        public void clickOnFromTopic() {
                log.info("Click on link create topic at screen home");
                SeleniumUtils.click(btnFormTopic);
        }

        public void inputTopic(String topic) {
                log.info("Input topic"+ nameTopic);
                SeleniumUtils.sendKeys(nameTopic,topic);
        }

        public void clickSubmitTopic() {
                log.info("Click on link submit topic ");
                SeleniumUtils.click(btnSubmitTopic);
        }

        public void verifyCreateTopicInList(String Text) {
                log.info("Created topic successfully ");
                SeleniumUtils.verifyDisplayInList(listNameTopics,Text);
        }

}
