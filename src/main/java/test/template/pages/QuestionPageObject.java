package test.template.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import test.template.common.BasePage;
import test.template.common.Config;
import test.template.utils.SeleniumUtils;

import java.util.List;

public class QuestionPageObject extends BasePage {
    private By linkTextQuestion = By.xpath("//a[@href ='/questions']");
    private By btnFormQuestion = By.xpath("//a[contains(@class,'ButtonAddNewQuestions')]");
    private By txtQuestion = By.id("question");
    private By txtDescription = By.id("description");
    private By selectTypeQuestion = By.id("questionType");
    private By btnSubmit = By.id("addQuestion");
    private By btnDeleteQuestion = By.xpath("//button[contains(@class,'ButtonDeleteQuestion')]");
    private By creationSuccessfulMessage = By.xpath("//input[@type='submit']");
    private By listNameQuestions= By.xpath("//tbody/tr[*]/td[3]");
    private By listNameDescription= By.xpath("//tbody/tr[*]/td[4]");
    private By listNameRow= By.xpath("//tbody/tr[*]");
    private String editQuestionButton = "//td[(text()='%s')]/following-sibling::td//button[contains(@class,'ButtonEditQuestion')]";

    public void createQuestion(String question, String description, String typeQuestion ) {
        clickOnLinkQuestion();
        clickOnFormQuestion();
        inputQuestion(question);
        inputDescription(description);
        selectTypeQuestion(typeQuestion);
        clickOnSubmitQuestion();
    }


    public void inputInformationQuestion(String question, String description, String typeQuestion ) {
        inputQuestion(question);
        inputDescription(description);
        selectTypeQuestion(typeQuestion);

    }
    public void clickOnLinkQuestion() {
        log.info("Click on link question at screen home");
        SeleniumUtils.click(linkTextQuestion);
    }

    public void clickOnFormQuestion() {
        log.info("Click on create question button");
//        SeleniumUtils.scrollElementIntoView(btnFormQuestion);
        SeleniumUtils.click(btnFormQuestion);
    }

    public void inputQuestion(String question) {
        log.info("Input question " + question);
        SeleniumUtils.sendKeys(txtQuestion,question );
    }

    public void inputDescription(String description) {
        log.info("Input description "+ description);
        SeleniumUtils.sendKeys(txtDescription, description);
    }

    public void selectTypeQuestion(String typequestion) {
        log.info("select type "+ typequestion);
        SeleniumUtils.selectDropdown(selectTypeQuestion,typequestion);
    }

    public void clickOnCancel() {
        log.info("Click on cancel button");
        SeleniumUtils.click(btnDeleteQuestion);
    }

    public void clickOnSubmitQuestion() {
        log.info("Click on submit question button");
        SeleniumUtils.click(btnSubmit);
    }
    public void verifyEmptyQuestion(String expected) {
        log.info("Verify the empty question and show empty tooltip ");
        System.out.println(Config.driver().findElement(txtQuestion).getAttribute("validationMessage"));
        String actual= Config.driver().findElement(txtQuestion).getAttribute("validationMessage");
        Assert.assertEquals(actual,expected);
    }

    public void verifyCreatQuestionInList(String message) {
        log.info("Verify the message of the created question: " + message);
        SeleniumUtils.verifyDisplayInList(listNameQuestions,message );
    }
    public void verifyInformationQuestionInList(String expectedQuestion,  String expectedDescription) {
        log.info("Verify the message of the created question: " );
        verifyRowInList(listNameRow,listNameQuestions, listNameDescription, expectedQuestion, expectedDescription);
    }

    public void verifyRowInList(By by, By elementQuestion, By elementDescription, String expectedQuestion,  String expectedDescription) {
        log.info("Verify name of the created question: {}", expectedQuestion);
        log.info("Verify name of the created description: {}", expectedDescription);
            List<WebElement> myElements = Config.driver().findElements(by);
            log.info("Total items: {}", myElements.size());

            for (WebElement row : myElements) {
                WebElement question = row.findElement(elementQuestion);
                WebElement description = row.findElement(elementDescription);
                log.info("check row: {} <> {}, {} <> {}", question.getText(), expectedQuestion, description.getText(), expectedDescription);
                if (question.getText().trim().equals(expectedQuestion)
                        && description.getText().trim().equals(expectedDescription)) {
                    return;
                }
            }
            log.info("Not found");
            Assert.fail();
    }

    public void clickEditQuestionButton(String questionName) {
        log.info("Click edit question button");
        String editButton = String.format(editQuestionButton, questionName);
        SeleniumUtils.click(By.xpath(editButton));
    }

    public void verifyEditedQuestionSuccess(String editedQuestionName) {
        SeleniumUtils.verifyDisplayInList(listNameQuestions, editedQuestionName);
    }

}

