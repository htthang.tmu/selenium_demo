package test.template.pages;

import org.openqa.selenium.By;
import org.testng.Assert;
import test.template.common.BasePage;
import test.template.dataObjects.AccountInfo;
import test.template.utils.SeleniumUtils;

public class RegisterPageObject extends BasePage {
    private By submitButton = By.xpath("//button[@type='submit']");
    private By nameEmptyWarningMessage = By.id("txtFirstname-error");
    private By emailEmptyWarningMessage = By.id("txtEmail-error");
    private By confirmEmailEmptyWarningMessage = By.id("txtCEmail-error");
    private By passwordWarningMessage = By.id("txtPassword-error");
    private By confirmPasswordEmptyWarningMessage = By.id("txtCPassword-error");
    private By phoneEmptyWarningMessage = By.id("txtPhone-error");
    private By nameTextBox = By.id("txtFirstname");
    private By confirmEmailTextBox = By.id("txtCEmail");
    private By emailTextBox = By.id("txtEmail");
    private By confirmationPasswordTextBox= By.id("txtCPassword");
    private By passwordTextBox= By.id("txtPassword");
    private By phoneTextBox= By.id("txtPhone");
    private By successfulMessage= By.id("thongbao");


    public void registerAccountWithoutDataObject(String name, String email, String confirmEmail, String password,
                                                 String confirmPassword, String phoneNumber) {
        inputName(name);
        inputEmail(email);
        inputConfirmEmail(confirmEmail);
        inputPassword(password);
        inputConfirmPassword(confirmPassword);
        inputPhoneNumber(phoneNumber);
        clickOnSubmit();
    }

    public void registerAccountWithDataObject(AccountInfo accountInfo) {
        log.info("Register account with data object");
        inputName(accountInfo.getName());
        inputEmail(accountInfo.getEmail());
        inputConfirmEmail(accountInfo.getConfirmEmail());
        inputPassword(accountInfo.getPassword());
        inputConfirmPassword(accountInfo.getConfirmPassword());
        inputPhoneNumber(accountInfo.getPhone());
        clickOnSubmit();
    }

    public void verifyNameWarningMessage(String message) {
        log.info("Verify name warning message: " + message);
        String actualMessage = driver.findElement(nameEmptyWarningMessage).getText();
        Assert.assertEquals(actualMessage, message);
    }

    public void verifyEmailWarningMessage(String message) {
        log.info("Verify email warning message: " + message);
        String actualMessage = driver.findElement(emailEmptyWarningMessage).getText();
        Assert.assertEquals(actualMessage, message);
    }

    public void verifyConfirmEmailWarningMessage(String message) {
        log.info("Verify confirm empty warning message: " + message);
        String actualMessage = driver.findElement(confirmEmailEmptyWarningMessage).getText();
        Assert.assertEquals(actualMessage, message);
    }

    public void verifyPasswordWarningMessage(String message) {
        log.info("Verify password warning message: " + message);
        String actualMessage = driver.findElement(passwordWarningMessage).getText();
        Assert.assertEquals(actualMessage, message);
    }

    public void verifyConfirmPasswordWarningMessage(String message) {
        log.info("Verify confirm password warning message: " + message);
        String actualMessage = driver.findElement(confirmPasswordEmptyWarningMessage).getText();
        Assert.assertEquals(actualMessage, message);
    }

    public void verifyPhoneWarningMessage(String message) {
        log.info("Verify phone warning message: " + message);
        String actualMessage = driver.findElement(phoneEmptyWarningMessage).getText();
        Assert.assertEquals(actualMessage, message);
    }

    public void clickOnSubmit() {
        log.info("Click on submit button");
        SeleniumUtils.click(submitButton);
    }

    private void inputName(String name) {
        log.info("Input name: " + name);
        SeleniumUtils.sendKeys(nameTextBox, name);
    }

    private void inputEmail(String email) {
        log.info("Input email: " + email);
        SeleniumUtils.sendKeys(emailTextBox, email);
    }

    private void inputConfirmEmail(String email) {
        log.info("Input confirm email: " + email);
        SeleniumUtils.sendKeys(confirmEmailTextBox, email);
    }

    private void inputPassword(String password) {
        log.info("Input password: " + password);
        SeleniumUtils.sendKeys(passwordTextBox, password);
    }

    private void inputConfirmPassword(String password) {
        log.info("Input confirm password: " + password);
        SeleniumUtils.sendKeys(confirmationPasswordTextBox, password);
    }

    private void inputPhoneNumber(String phoneNumber) {
        log.info("Input phone number: " + phoneNumber);
        SeleniumUtils.sendKeys(phoneTextBox, phoneNumber);
    }

}


