package test.template.pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import test.template.common.BasePage;
import test.template.common.Config;
import test.template.utils.SeleniumUtils;

import java.util.List;

public class DeletePageObject extends BasePage {
    private final String btnDeleteQuestion = "//td[(text()='%s')]/following-sibling::td//button[@id= 'deleteBtn']";

    private By listNameQuestions= By.xpath("//tbody/tr[*]/td[3]");


    public void verifyDeleteInList(String text) {
        log.info("Deleted successfully");
        verifySuccessfullyDeletedTheQuestion(listNameQuestions,text);

    }

    public void verifySuccessfullyDeletedTheQuestion(By by, String expectedValue ) {
        log.info("Display in list");
        List<WebElement> myElements = Config.driver().findElements(by);
        System.out.println("Tổng số items: " +myElements.size());
        for (WebElement element : myElements) {
            if (element.getText().trim().equals(expectedValue)) {
                Assert.fail();
                return;
            }
        }
        System.out.println("Deleted success");
    }


    public void questionName(String text) {
        log.info("Input name question to find element");
        String button = String.format(btnDeleteQuestion, text);
        By buttonDelete= By.xpath(button);
        SeleniumUtils.click(buttonDelete);
    }


}
