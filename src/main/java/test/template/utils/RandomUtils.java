package test.template.utils;

import java.util.Random;

public final class RandomUtils {
    private static String ORIGIN_STRING = "ABCDEFGHIJKLMNOPQRSTUWXYZabcdefghijklmnopqrstuwxyz";

    public static String getAlphaString() {
        StringBuilder sb = new StringBuilder();

        for (int i=0; i < 10; i++) {
            int index = (int) (ORIGIN_STRING.length() * Math.random());
            sb.append(ORIGIN_STRING.charAt(index));
        }

        return sb.toString();
    }

    public static String getNumeric(int digit) {
        // It will generate 6 digit random Number.
        // from 0 to 999999
        Random rnd = new Random();
        int number = rnd.nextInt(999999);

        // this will convert any number sequence into 6 character.
        return String.format("%" + digit + "d", number);
    }

    public static String getPhoneNumber() {
        // It will generate 9 digit random Number.
        // from 0 to 999999
        Random rnd = new Random();
        int number = rnd.nextInt(999999);

        // this will convert any number sequence into 9 character.
        return String.format("%09d", number);
    }
}
