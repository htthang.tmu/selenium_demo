package test.template.cases.Ivy;
import org.testng.annotations.Test;
import test.template.cases.base.BaseWebTestCase;

public class Login extends BaseWebTestCase {
    @Test
    public void openBrower() {
        browserURL("https://link-staging.saas.ivy.com/auth/login");
    }

    @Test(priority = 1)
    public void login_with_password() {
        loginPageObject.loginWithPassWord("hani+090801@offspringdigital.com","12345qQ@");
        loginPageObject.verifyTextTenant("Select Account");
    }
    @Test(priority = 2)
    public void select_tenant() {
        loginPageObject.selectTenant();
    }

}
