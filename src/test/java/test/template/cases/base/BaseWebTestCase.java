package test.template.cases.base;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;
import test.template.common.Config;
import test.template.pages.*;
import test.template.pages.LoginPageObject;


public abstract class BaseWebTestCase extends BaseTestCase {
	protected Logger log = LoggerFactory.getLogger(this.getClass());

	public LoginPageObject loginPageObject= new LoginPageObject();

	public RegisterPageObject registerObject = new RegisterPageObject();

	public LoginHitachiPageObject loginObject=new LoginHitachiPageObject(); ;

	public TopicPageObject topicObject =new TopicPageObject();


	public DeletePageObject deleteObject =new DeletePageObject(); ;

	public QuestionPageObject questionObject;

	// A Test Suite may have many tests.
	// A Test may run many test methods in one or more classes.
	// Before a Test startup, setup the browser and environment.
	// When the Test finished, close the browser.
	@BeforeTest
	@Parameters({ "chrome", "url" })
	public void setupDriver(@Optional String browser, @Optional String url) throws Exception {
		Config.setupWebDriver(browser, url);
		log.info("Setup web driver: " + browser);
		Init();
	}
	
	@AfterClass
	public void tearDown() {
		super.tearDown();
		if (Config.driver() != null) {
			Config.driver().quit();	
		}
	}

	protected RemoteWebDriver driver() {
		return Config.driver();
	}

	protected void Init() {
		log.info("Initialize");
		registerObject = new RegisterPageObject();
	}

	protected void QuestionPageObject() {
		log.info("Initialize");
		questionObject = new QuestionPageObject();
	}
	public void browserURL(String url) {
		driver().get(url);
	}
}
