package test.template.cases.demo_project;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import test.template.cases.base.BaseWebTestCase;
import test.template.utils.RandomUtils;
import test.template.utils.SeleniumUtils;

public class DeleteQuestion extends BaseWebTestCase {
    @BeforeTest
    public void beforeTest(){

        driver().get("http://hitachi2-demo.os.merch8.com/login");
        loginObject.loginHitachi("user", "password");
        SeleniumUtils.takeScreenShot();
    }

    @Test()
    public void deleteSuccessfulQuestion()  {
        String questionName = RandomUtils.getAlphaString();
        questionObject.createQuestion(questionName,"Input description", "2");
        deleteObject.questionName(questionName);
        deleteObject.verifyDeleteInList(questionName);
    }

    @Test()
    public void deleteFailQuestion()  {
        String questionName = RandomUtils.getAlphaString();
        questionObject.createQuestion(questionName,"Input description", "2");
        deleteObject.questionName(questionName);
        deleteObject.verifyDeleteInList("97fe949f-8e0a-4aa5-90c4-b13b983a89f4");
    }


}
