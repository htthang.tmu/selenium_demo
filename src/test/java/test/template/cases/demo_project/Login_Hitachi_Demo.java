package test.template.cases.demo_project;

import org.testng.annotations.Test;
import test.template.cases.base.BaseWebTestCase;

public class Login_Hitachi_Demo extends BaseWebTestCase {
    @Test
    public void LaunchBrowser() {
        browserURL("http://hitachi2-demo.os.merch8.com/login");
    }

    @Test(priority = 1)
    public void Login_to_hitachi_system_successfully() {
        loginObject.loginHitachi("user", "password");
    }

}
