package test.template.cases.demo_project;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import test.template.cases.base.BaseWebTestCase;
import test.template.utils.RandomUtils;
import test.template.utils.SeleniumUtils;

import java.util.UUID;


public class CreateQuestions extends BaseWebTestCase {
    @BeforeTest
    public void beforeTest(){

        driver().get("http://hitachi2-demo.os.merch8.com/login");
        loginObject.loginHitachi("user", "password");
        SeleniumUtils.takeScreenShot();
    }

    @Test(priority = 1)
    public void createSuccessfulQuestion() {
        String nameQuestion = RandomUtils.getAlphaString();
        String nameDescription = RandomUtils.getAlphaString();
        questionObject.clickOnLinkQuestion();
        questionObject.clickOnFormQuestion();
        questionObject.inputInformationQuestion(nameQuestion, nameDescription, "1");
        questionObject.clickOnSubmitQuestion();
        questionObject.verifyInformationQuestionInList(nameQuestion,nameDescription);
    }

    @Test(priority = 2)
    public void TC_02_checkEmptyQuestionAndDescription() {
        questionObject.clickOnLinkQuestion();
        questionObject.clickOnFormQuestion();
        questionObject.inputInformationQuestion("","","1");
        questionObject.clickOnSubmitQuestion();
        questionObject.verifyEmptyQuestion("Please fill out this field.");
    }

    @Test(priority = 3)
    public void TC_03_checkEmptyDescription() {
        String nameQuestion = RandomUtils.getAlphaString();
        String nameDescription = "";
        questionObject.clickOnLinkQuestion();
        questionObject.clickOnFormQuestion();
        questionObject.inputInformationQuestion(nameQuestion,nameDescription,"1");
        questionObject.clickOnSubmitQuestion();
        questionObject.verifyInformationQuestionInList(nameQuestion,nameDescription);
    }

    @Test(priority = 4)
    public void TC_04_checkEmptyQuestion() {
        questionObject.clickOnLinkQuestion();
        questionObject.clickOnFormQuestion();
        questionObject.inputInformationQuestion("","Once this is done you can select.","1");
        questionObject.clickOnSubmitQuestion();
        questionObject.verifyEmptyQuestion("Please fill out this field.");
    }
}
