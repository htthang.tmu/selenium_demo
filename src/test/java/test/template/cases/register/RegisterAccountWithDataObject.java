package test.template.cases.register;
import org.testng.annotations.Test;
import test.template.cases.base.BaseWebTestCase;
import test.template.dataObjects.AccountInfo;

public class RegisterAccountWithDataObject extends BaseWebTestCase {

    @Test
    public void LaunchBrowser() {
        browserURL("https://alada.vn/tai-khoan/dang-ky.html");
    }

    @Test(priority = 1)
    public void register_with_empty_data() {
        registerObject.clickOnSubmit();

        registerObject.verifyNameWarningMessage("Vui lòng nhập họ tên");
        registerObject.verifyEmailWarningMessage("Vui lòng nhập email");
        registerObject.verifyConfirmEmailWarningMessage("Vui lòng nhập lại địa chỉ email");
        registerObject.verifyPasswordWarningMessage("Vui lòng nhập mật khẩu");
        registerObject.verifyConfirmPasswordWarningMessage("Vui lòng nhập lại mật khẩu");
        registerObject.verifyPhoneWarningMessage("Vui lòng nhập số điện thoại.");
    }

//    @Test(priority = 2)
//    public void Register_With_Email_Invalidate() {
//        AccountInfo accountInfo = new AccountInfo();
//        accountInfo.setEmail("Nancygmail.com");
//        accountInfo.setConfirmEmail("Nancygmail.com");
//
//        registerObject.registerAccountWithDataObject(accountInfo);
//
//        registerObject.verifyEmailWarningMessage("Vui lòng nhập email hợp lệ");
//        registerObject.verifyConfirmEmailWarningMessage("Email nhập lại không đúng");
//    }

//    @Test(priority = 3)
//    public void register_with_incorrect_confirm_email() {
//        AccountInfo accountInfo = new AccountInfo();
//        accountInfo.setEmail("1Nancy@gmail.com");
//
//        registerObject.registerAccountWithDataObject(accountInfo);
//
//        registerObject.verifyConfirmEmailWarningMessage("Email nhập lại không đúng");
//    }

//    @Test(priority = 4)
//    public void register_with_password_less_than_six_characters() {
//        AccountInfo accountInfo = new AccountInfo();
//        accountInfo.setPassword("1234");
//        accountInfo.setConfirmPassword("1234");
//
//        registerObject.registerAccountWithDataObject(accountInfo);
//
//        registerObject.verifyConfirmPasswordWarningMessage("Mật khẩu phải có ít nhất 6 ký tự");
//    }

}
